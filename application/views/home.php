
<div class="container">
<div id="search_boxes">
	
	<div class="searchBox bd-example">
	<h3 class="searchBoxHead title">Search Grade Section</h3>
		<form action="<?php echo base_url('/users/search');?>" method="post" name="gradeSearch" id="gradeSearch">
		<input type="hidden" name="operation" value="name">
		<div class="form-group">
		<input type="radio" name="region" value="5" class="radiobutton form-check"><strong>&nbsp;North American</strong> - <a href="<?php echo base_url('users/login');?>">Login</a></br>
		<input type="radio" name="region" value="4" class="radiobutton  form-check"><strong>&nbsp;European Limited</strong> - <a href="<?php echo base_url('users/login');?>">Login</a></br>
		<input type="radio" name="region" value="0" class="radiobutton  form-check" checked="checked">&nbsp;All Available
		</div>
		<div class="form-group">
			<input type="text"  placeholder="Enter Grade Name" name="name" id="name" class="form-control" onkeydown="if ((event.which &amp;&amp; event.which == 13) || (event.keyCode &amp;&amp; event.keyCode == 13)) {document.gradeSearch.nameBtn.click();return false;} else return true;">
			<span class="form-highlight"></span>
        <span class="form-bar"></span>
			<label class="muted form-text">Enter all or part of the Grade Name</label>
		</div>
		<div class="form-group">
			<input id="classification" type="text" name="classification" value="" class="form-control" placeholder="Enter Classication" onkeydown="if ((event.which &amp;&amp; event.which == 13) || (event.keyCode &amp;&amp; event.keyCode == 13)) {document.gradeSearch.classBtn.click();return false;} else return true;">
			<span class="form-highlight"></span>
        <span class="form-bar"></span>
			<label class="muted form-text">Enter all or part of the Classificatione</label>
		</div>
		<input class="btn btn-primary pull-right" type="submit" value="Search">
		</br>
		<a href="search_grades.php#howcan">How can the Grade Section help me?</a>
		</form>
	</div>



</em></div>

<div id="company_search">
	<div class="searchBox bd-example">
	<h3 class="searchBoxHead title">Search Company Section</h3>
		<form action="<?php echo base_url('/users/searchcompany');?>" method="post" name="companySearch" id="companySearch">
		<input type="hidden" name="operation" value="name">
		<div class="form-group">
		<input type="radio" name="region_comp" value="5" class="radiobutton form-check"><strong>&nbsp;North American</strong> - <a href="<?php echo base_url('users/login');?>">Login</a></br>
		<input type="radio" name="region_comp" value="4" class="radiobutton  form-check"><strong>&nbsp;European Limited</strong> - <a href="<?php echo base_url('users/login');?>">Login</a></br>
		<input type="radio" name="region_comp" value="0" class="radiobutton  form-check" checked="checked">&nbsp;All Available
		</div>
		<div class="form-group">
			<input type="text"  placeholder="Enter Company Name" name="name" id="name" class="form-control" onkeydown="if ((event.which &amp;&amp; event.which == 13) || (event.keyCode &amp;&amp; event.keyCode == 13)) {document.gradeSearch.nameBtn.click();return false;} else return true;">
			<span class="form-highlight"></span>
        <span class="form-bar"></span>
			<label class="muted form-text">Enter all or part of the Company Name</label>
		</div>
		
		<input class="btn btn-primary pull-right" type="submit" value="Search">
		</br>
		<a href="search_company.php#howcan">How can the Company Section help me?</a>
		</form>
	</div>



</em></div>

</div>