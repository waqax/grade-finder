<?= $form->messages(); ?>
<?php  
if(isset($_GET['AccountNo'])){
	 $AccountNo = $_GET['AccountNo'];
		$query_company = $this->db->get_where('companies', array('AccountNo' => $AccountNo ));
		$result_company = $query_company ->result();
		$CountryID = $result_company[0]->CountryID;
		$StateID = $result_company[0]->StateID;
}

if(isset($_GET['office'])){
	 $OfficeID = $_GET['office'];
}


?>
<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				<?= $form->open(); ?>
                
                	<?= $form->bs3_text('Company', 'Company','',array('required' => 'required')); ?>
					<?= $form->bs3_text('Division', 'Division','',array('required' => 'required')); ?>
					<?= $form->bs3_text('Address', 'Address1'); ?>
                    
                    <div class="form-group" style="width:60%; float:left; "><label for="CategoryNumber">Country:</label>
                    <select name="CountryID" class="chosen-select" data-placeholder="Select CountryID" >
                    <?php foreach ($Countries as $Country): ?>
                    <?php if($Country->id == $CountryID){ $selected = 'selected="selected"';}
							else{$selected =  '';}
					?>
                      <option value="<?= $Country->id; ?>" <?= $selected ?>><?= $Country->CountryName; ?></option>
                    <?php endforeach; ?>
                    </select></div>
                
                    <div class="form-group" style="width:40%; float:left; "><label for="CategoryNumber">States:</label>
                        <select name="StateID" class="chosen-select" data-placeholder="Select States" >
                    <?php foreach ($states as $state): ?>
                    <?php if($state->id == $StateID){ $selected = 'selected="selected"';}
							else{$selected =  '';}
					?>
                          <option value="<?= $state->id; ?>" <?= $selected ?>><?= $state->StateCode; ?></option>
                        <?php endforeach; ?>
                    </select></div>
                    
                    <?= $form->bs3_text('Zip', 'ZipPostalCode','',array('required' => 'required')); ?>
					<?= $form->bs3_text('Phone', 'Phone1','',array('required' => 'required')); ?>
					<?= $form->bs3_text('Fax', 'Fax'); ?>
                    <?= $form->bs3_text('EMail', 'EMail','',array('required' => 'required')); ?>
					<?= $form->bs3_text('URL', 'HomePage'); ?>
					<?= $form->bs3_text('Certification', 'Certification'); ?>
                    <?= $form->bs3_textarea('Company Description', 'companyDescription'); ?>
                    
                    <div>Personnel Information:</div>
					<?= $form->bs3_text('', 'Personnel_1'); ?>
                    <?= $form->bs3_text('', 'Personnel_2'); ?>
                    <?= $form->bs3_text('', 'Personnel_3'); ?>
                    <?= $form->bs3_text('', 'Personnel_4'); ?>
                    <?= $form->bs3_text('', 'Personnel_5'); ?>
                    
                    <div>Record Control Items</div>
                	<?= $form->bs3_text('Parent ID', 'parentID',$AccountNo); ?>
                    <?= $form->bs3_text('AdPgNo', 'AdPgNo'); ?>
                    <?= $form->bs3_text('Sort', 'Sort'); ?>
                    
                    <div class="form-group" style="width:20%; float:left; ">
                    	<input type="checkbox" name="Grades" value="1"> Grade
                    </div>
                    
                    <div class="form-group" style="width:20%; float:left; ">
                    	<input type="checkbox" name="globalheadquarters" value="1"> Global Hdqts
                    </div>
                    
                    <div class="form-group" style="width:60%; float:left; ">
                    	<input type="checkbox" name="corporateheadquarters" value="1"> Corporate Hdqts
                    </div>
                    
                    
                    <div class="form-group" style="width:50%; float:left; "><label for="CategoryNumber">Section:</label>
                        <select name="Section" class="chosen-select" data-placeholder="Select section" >
                        <?php foreach ($sections as $section): ?>
                          <option value="<?= $section->id; ?>"><?= $section->sectionName; ?></option>
                        <?php endforeach; ?>
                    </select></div>
                    
                    
                    <div class="form-group" style="width:50%; float:left; "><label for="CategoryNumber">Office:</label>
                        <select name="Office" class="chosen-select" data-placeholder="Select States" >
                        <?php foreach ($offices as $office): ?>
                        <?php if($office->id == $OfficeID){ $selected = 'selected="selected"';}
							else{$selected =  '';}
						?>
                          <option value="<?= $office->id; ?>" <?= $selected ?>><?= $office->OfficeName; ?></option>
                        <?php endforeach; ?>
                    </select></div>
                
 
					<?= $form->bs3_submit("Add"); ?>
					
				<?= $form->close(); ?>
			</div>
		</div>
	</div>
	
</div>