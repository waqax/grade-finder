<?php echo $form->messages(); ?>
<?php  
if(isset($_GET['regionID'])){
	$regionID = $_GET['regionID'];
}
?>
<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				<?php echo $form->open(); ?>
                <?php if(isset($_GET['regionID'])){
					$query = $this->db->get_where('regions', array('id' => $regionID ));
					$result = $query->result();
					?>
                
                <input type="hidden" name="region_id" id="region_id" value="<?php echo $regionID; ?>"/>
                <div class="form-group"><label for="CategoryNumber">Region:</label>
                	<?php echo  $result[0]->RegionName; ?>
				</div>
                <?php }else{ ?>
                <div class="form-group"><label for="CategoryNumber">Region:</label>
                    <select name="region_id" class="chosen-select" data-placeholder="Select Region" >
                    <?php foreach ($regions as $region): ?>
                      <option value="<?php echo $region->id; ?>"><?php echo $region->RegionName; ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
                <?php } ?>
					<?php echo $form->bs3_text('Category No', 'CategoryNumber','',array('required' => 'required')); ?>
					<?php echo $form->bs3_text('Category Name', 'CategoryName','',array('required' => 'required')); ?>
					<?php echo $form->bs3_text('Basis Size', 'BasisSize'); ?>

					<?php echo $form->bs3_submit("Add"); ?>
					
				<?php echo $form->close(); ?>
			</div>
		</div>
	</div>
	
</div>