<div class="panel-group" id="accordion">
    
    <?php foreach($regions as $region){ 
	 		$query = $this->db->get_where('categories', array('RegionID' => $region->id));
			$results = $query->result();
	?>
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $region->id; ?>"><?= $region->RegionName; ?></a>
                	<a class="pull-right"  href="<?= base_url(); ?>admin/category/create?regionID=<?= $region->id; ?>">add new category</a>
                    <span style="float:right; margin-right:5%;"><?= count($results); ?> Listings</span>
                </h4>
            </div>
            <div id="collapse<?= $region->id; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                   <?php foreach ($results as $row){
							echo "<li><a href='".base_url()."admin/category/index1/edit/".$row->CategoryNumber."'>".$row->CategoryNumber." | ".$row->CategoryName."</a></li>";
						}
				   ?>
                </div>
            </div>
        </div>
	
	<?php }?>
    
</div>