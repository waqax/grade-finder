<?php echo $form->messages(); ?>
<?php  
if(isset($_GET['AccountNo'])){
	$AccountNo = $_GET['AccountNo'];
}
?>
<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				<?php echo $form->open(); ?>
                <input type="hidden" name="AccountNo" id="AccountNo" value="<?php echo $AccountNo; ?>"/>
					<?php echo $form->bs3_text('Product Announcement Title', 'productAnnouncement_title','',array('required' => 'required')); ?>
					<?php echo $form->bs3_text('Product Announcement Link', 'productAnnouncement_link','',array('required' => 'required')); ?>
					<?php echo $form->bs3_textarea('Product Announcement Copy', 'productAnnouncement_copy'); ?>

					<?php echo $form->bs3_submit("Add"); ?>
					
				<?php echo $form->close(); ?>
			</div>
		</div>
	</div>
	
</div>