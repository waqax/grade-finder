<!-- Bootstrap FAQ - START -->
<?php //print_r($regions);?>

    <div class="panel-group" id="accordion">
    
    <?php foreach($regions as $region){ ?>
	
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $region->id; ?>"><?= $region->RegionName; ?></a>
                	<a class="pull-right"  href="<?= base_url(); ?>admin/category/create?regionID=<?= $region->id; ?>">add new category</a>
                </h4>
            </div>
            <div id="collapse<?= $region->id; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                   <?php //echo $region->RegionDescription; ?>
                   <?php $query = $this->db->get_where('categories', array('RegionID' => $region->id));
						foreach ($query->result() as $row){
							echo "<li><a href='".base_url()."admin/category/index1/edit/".$row->CategoryNumber."'>".$row->CategoryNumber." | ".$row->CategoryName."</a></li>";
						}
				   ?>
                </div>
            </div>
        </div>
	
	<?php }?>
    
    </div>
