<?php
if($results){ 
$count_all = count($results);

if($count_all == 1){ ?>
<div class="col-md-12">
  <?php 
	$query_office = $this->db->get_where('offices', array('id' => $results[0]->Office));
	$get_office = $query_office->result();
?>
  <h2>
    <?= $get_office[0]->OfficeName; ?>
  </h2>
  <a class="pull-right" href="<?= base_url(); ?>admin/company/index/edit/<?= $results[0]->AccountNo; ?>">
  <button>Edit</button>
  </a> </div>
<div class="col-md-12">
  <table>
    <?php  foreach($results as $row){ ?>
    <tr>
      <th>AccountNo: </th>
      <td><?= $row->AccountNo;?></td>
    </tr>
    <tr>
      <th>Name:</th>
      <td><?= $row->Company;?></td>
    </tr>
    <tr>
      <th>Division:</th>
      <td><?= $row->Division;?></td>
    </tr>
    <tr>
      <th>Address:</th>
      <td><?= $row->Address1;?></td>
    </tr>
    <tr>
      <th></th>
      <td><?= $row->CityArea.", ".$row->StateCode." ".$row->ZipPostalCode;?></td>
    </tr>
    <tr>
      <th>Phone:</th>
      <td><?= $row->Phone1;?></td>
    </tr>
    <tr>
      <th>Fax:</th>
      <td><?= $row->Fax;?></td>
    </tr>
    <tr>
      <th>EMail:</th>
      <td><?= $row->EMail;?></td>
    </tr>
    <tr>
      <th>URl:</th>
      <td><?= $row->HomePage;?></td>
    </tr>
    <tr>
      <th>Certification:</th>
      <td><?= $row->Certification;?></td>
    </tr>
    <tr>
      <th>Personnel:</th>
      <td><?= $row->Personnel_1;?></td>
    </tr>
    <tr>
      <th></th>
      <td><?= $row->Personnel_2;?></td>
    </tr>
    <tr>
      <th></th>
      <td><?= $row->Personnel_3;?></td>
    </tr>
    <tr>
      <th></th>
      <td><?= $row->Personnel_4;?></td>
    </tr>
    <tr>
      <th></th>
      <td><?= $row->Personnel_5;?></td>
    </tr>
    <tr>
      <th>Description:</th>
      <td><?= $row->companyDescription;?></td>
    </tr>
    <?php   } ?>
  </table>
</div>
<br/>
<div class="clearfix"></div>
<div class="panel-group" id="accordion">
  <?php $query_grade = $this->db->get_where('grades', array('AccountNo1' => $results[0]->AccountNo));
        $result_grades = $query_grade->result() ;
   ?>
  <!--<div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-grades">Grades</a>
                	<a class="pull-right"  href="<?= base_url(); ?>admin/category/create?regionID=">add new</a>
                    <span style="float:right; margin-right:5%;"><?= count($result_grades); ?> Listings</span>
                </h4>
            </div>
            <div id="collapse-grades" class="panel-collapse collapse">
                <div class="panel-body">
                   <?php foreach ($result_grades as $result_grade){
							echo $result_grade->GradeName;
						}
				   ?>
                </div>
            </div>
       </div>-->
  	<?php	/*if ($this->ion_auth->in_group(array('webmaster','regional_super_user', 'admin'))){
				$db_array =  array('AccountNo' => $results[0]->AccountNo);
			}else{
				$db_array = array('AccountNo' => $results[0]->AccountNo,'active' => '1');
				}*/
	$query_productannouncement = $this->db->get_where('productannouncement', array('AccountNo' => $results[0]->AccountNo));
        	$result_productannouncements = $query_productannouncement->result() ;
        ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"> 
      	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse-product-announcements"> Product Announcements</a> 
        <a class="pull-right"  href="<?= base_url(); ?>admin/productannouncement/create?AccountNo=<?= $results[0]->AccountNo; ?>">add new</a> 
        <span style="float:right; margin-right:5%;"> <?= count($result_productannouncements); ?> Listings</span> 
        <?php foreach ($result_productannouncements as $check_result){ 
				$check_active[] =  $check_result->active;
			}
			if($result_productannouncements){
			if (in_array("0", $check_active)){
	 	?>
        <span style="float:right; margin-right:1%;"> <i class="fa fa-dot-circle-o" aria-hidden="true"></i></span>
        <?php }}?> 
      </h4>
    </div>
    <div id="collapse-product-announcements" class="panel-collapse collapse">
      <div class="panel-body">
        <?php	foreach ($result_productannouncements as $result_productannouncement){ ?>
        <table>
          <tr>
            <td width="85%;"><b><?= $result_productannouncement->productAnnouncement_title; ?></b></td>
            <td width="15%;">
           <?php  if ($this->ion_auth->in_group(array('webmaster','regional_super_user', 'admin'))){ ?>
           	<?php  if($result_productannouncement->active == "0"){ ?>
				<a href="<?= base_url(); ?>admin/productannouncement/index/delete/<?= $result_productannouncement->id; ?>" onclick="return confirm('Are you sure you want cancel corrections and REVERT to the original version?');">
                <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                
            	<a href="<?= base_url(); ?>admin/productannouncement/approved/<?= $result_productannouncement->id; ?>">
                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>&nbsp;&nbsp;
            	<?php }} ?>
                
                <?php if($result_productannouncement->active == "0"){ ?>
                	<span style="background-color:#E39772;color:#660000;">&nbsp;ADDITION&nbsp;PENDING&nbsp;</span>
                <?php } ?>
                
				<a href="<?= base_url(); ?>admin/productannouncement/index/edit/<?= $result_productannouncement->id; ?>">Edit</a>
                <!--<a href="#" title="Delete Company" onclick="return confirm('Are you sure you want to delete this item?');" class="delete-row">
                <i class="fa fa-minus-circle" aria-hidden="true"></i></a>-->
                </td>
          </tr>
          <tr>
            <td><?= $result_productannouncement->productAnnouncement_link; ?></td>
          </tr>
          <tr>
            <td><?= $result_productannouncement->productAnnouncement_copy; ?></td>
          </tr>
        </table>
        <br/>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php
	$query_offices = $this->db->get_where('offices');
	$result_offices = $query_offices->result();
	foreach($result_offices as $result_office){ 
			$get_query = $this->db->get_where('companies', array('parentID' => $results[0]->AccountNo ,'Office' => $result_office->id));
			$get_results = $get_query->result();
 ?>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title"> 
      	<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $result_office->id; ?>"><?= $result_office->OfficeName; ?></a>
        <a class="pull-right"  href="<?= base_url(); ?>admin/company/create?AccountNo=<?= $results[0]->AccountNo; ?>&office=<?= $result_office->id; ?>">add new</a> 
        <span style="float:right; margin-right:5%;"><?= count($get_results); ?> Listings</span>
        <?php foreach ($get_results as $check_result){ 
				$check_active_comp[] =  $check_result->active;
			}
			if($get_results){
			if (in_array("0", $check_active_comp)){
	 	?>
        <span style="float:right; margin-right:1%;"> <i class="fa fa-dot-circle-o" aria-hidden="true"></i></span>
        <?php }}?> 
      </h4>
    </div>
    <div id="collapse<?= $result_office->id; ?>" class="panel-collapse collapse">
      <div class="panel-body">
        <?php  $count = 0; foreach ($get_results as $get_result){ 
				   		$color = ($count % 2 === 0) ? '#eeeeee' : '#fff' ;
				   			$guery_state = $this->db->get_where('states', array('id' => $get_result->StateID ));
							$results_states = $guery_state->result();
							if(isset($results_states[0]->StateCode))$state = $results_states[0]->StateCode;
							else $state = "";
				   ?>
        <table style="background-color:<?= $color; ?>; width:60%;">
          <tr>
            <th>AccountNo: </th>
            <td><?= $get_result->AccountNo;?></td>
            <td width="25%">
            <?php  if ($this->ion_auth->in_group(array('webmaster','regional_super_user', 'admin'))){ ?>
           	<?php  if($get_result->active == "0"){ ?>
				<a href="<?= base_url(); ?>admin/company/index/delete/<?= $get_result->AccountNo; ?>" onclick="return confirm('Are you sure you want cancel corrections and REVERT to the original version?');">
                <i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;
                
            	<a href="<?= base_url(); ?>admin/company/approved/<?= $get_result->AccountNo; ?>">
                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>&nbsp;&nbsp;
            	<?php }} ?>
            <?php if($get_result->active == "0"){ ?>
                	<span style="background-color:#E39772;color:#660000;">&nbsp;ADDITION&nbsp;PENDING&nbsp;</span>
                <?php } ?>
            <a class="pull-right" href="<?= base_url(); ?>admin/company/index/edit/<?= $get_result->AccountNo; ?>">Edit</a>
            </td>
          </tr>
          <tr>
            <th>Name:</th>
            <td><?= $get_result->Company;?></td>
          </tr>
          <tr>
            <th>Division:</th>
            <td><?= $get_result->Division;?></td>
          </tr>
          <tr>
            <th>Address:</th>
            <td><?= $get_result->Address1;?></td>
          </tr>
          <tr>
            <th></th>
            <td><?= $get_result->CityArea.", ".$state ." ".$get_result->ZipPostalCode;?></td>
          </tr>
          <tr>
            <th>Phone:</th>
            <td><?= $get_result->Phone1;?></td>
          </tr>
          <tr>
            <th>Fax:</th>
            <td><?= $get_result->Fax;?></td>
          </tr>
          <tr>
            <th>EMail:</th>
            <td><?= $get_result->EMail;?></td>
          </tr>
          <tr>
            <th>URl:</th>
            <td><?= $get_result->HomePage;?></td>
          </tr>
          <tr>
            <th>Certification:</th>
            <td><?= $get_result->Certification;?></td>
          </tr>
          <tr>
            <th>Personnel:</th>
            <td><?= $get_result->Personnel_1;?></td>
          </tr>
          <tr>
            <th></th>
            <td><?= $get_result->Personnel_2;?></td>
          </tr>
          <tr>
            <th></th>
            <td><?= $get_result->Personnel_3;?></td>
          </tr>
          <tr>
            <th></th>
            <td><?= $get_result->Personnel_4;?></td>
          </tr>
          <tr>
            <th></th>
            <td><?= $get_result->Personnel_5;?></td>
          </tr>
          <tr>
            <th>Description:</th>
            <td><?= $get_result->companyDescription;?></td>
          </tr>
        </table>
        <br/>
        <?php $count++; } ?>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php }else{ ?>
<?php 
$search_text = get_cookie("company_search_text");
if($search_text != ""){
if($count_all >= "25"){?>
<b><i>Your search returned over 25 results. Please choose from the results below, or narrow your search criteria bellow.</i></b><br>

<div class="col-md-12">
    <form action="<?= $this->config->base_url(); ?>admin/searchcompany" method="POST" id="filtering_form" class="filtering_form" autocomplete="off" accept-charset="utf-8" >
        <div class="sDiv quickSearchBox" id="quickSearchBox">Company:
            <div class="sDiv2">
                Search: <input type="text" class="qsbsearch_fieldox search_text" name="search_text" size="30" id="search_text">
                    <select name="search_field" id="search_field">
                        <!--<option value="">Search all</option>-->
                        <option value="AccountNo">AccountNo</option>
                        <option value="Company">Company</option>
                        <!--<option value="Address1">Address</option>-->
                        <!--<option value="EMail">EMail</option>-->
                    </select>
                <input type="submit" value="Search" class="crud_search" name="crud_search" id="crud_search"><br/>
                Enter the ID and/or select option from dropdown
            </div>
        </div>
	</form>
</div>
<?php }} ?>
<?php	foreach ($results as $result){
				$query_country = $this->db->get_where('country', array('id' => $result->CountryID));
				$result_countries = $query_country->result();
				foreach ($result_countries as $result_country){
					$RegionIDs[] = $result_country->RegionID;
					$CountryIDs[] = $result->CountryID;
					$CompanyIDs[] = $result->AccountNo;
				}

}
$RegionID = array_unique($RegionIDs);
$CountryID = array_unique($CountryIDs);
$CompanyID = array_unique($CompanyIDs);

foreach ($RegionID as $Region){
$query_region = $this->db->get_where('regions', array('id' => $Region));
$result_regions = $query_region->result();
foreach ($result_regions as $result_region){ ?>
<h2>
  <?= $result_region->RegionName; ?>
  Region</h2>
<table width="100%" height="auto">
  <tr>
    <th>Company</th>
    <th>Area</th>
    <th>ID</th>
  </tr>
  <?php //$query_country = $this->db->get_where('country', array('RegionID' => $result_region->id));
			//$result_countries = $query_country->result();
					//foreach ($result_countries as $result_country){
						//if (in_array($result_country->id, $CountryID)) {
						//$query_company = $this->db->get_where('companies', array('CountryID' => $result_country->id));
						//$result_companies = $query_company->result(); 
							$this->db->select ( 'comp.*,regions.*,country.*' )
							->from ( 'companies as comp' )
							->join ( 'states state', 'comp.StateID = state.id ', 'left')
							//->join ( 'users_relation usr', 'usr.accountNo = comp.AccountNo')
							->join ( 'country', 'comp.CountryID = country.id ', 'right')
							->join ( 'regions', 'country.RegionID = regions.id ', 'right')
							->where('country.RegionID',$result_region->id)
							->where('parentID',NULL)
							//->where('Grades',"1")
							->order_by("Company","ASC");
							$query_company = $this->db->get();
							$result_companies = $query_company->result();
						
							foreach ($result_companies as $result_company){
								if (in_array($result_company->AccountNo, $CompanyID)) { 
								echo "<tr>";
									echo "<td width='50%'><a href='".base_url()."admin/searchcompany/index/".$result_company->AccountNo."'>".$result_company->Company."</a></td>";
									echo "<td width='25%'>".$result_company->CityArea."</td>";
									echo "<td width='25%'>".$result_company->AccountNo."</td>";
								echo "</tr>";
								}		
							}
						//}
					//}
	?>
</table>
<?php } } }?>
<?php } else{
echo "No Search record found";
 }
?>
