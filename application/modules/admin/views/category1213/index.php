<!-- Bootstrap FAQ - START -->
<?php //print_r($regions);?>

    <div class="panel-group" id="accordion">
    
    <?php foreach($regions as $region){ ?>
	
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $region->id; ?>"><?php echo $region->RegionName; ?></a>
                </h4>
            </div>
            <div id="collapse<?php echo $region->id; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                   <?php //echo $region->RegionDescription; ?>
                   <?php $query = $this->db->get_where('gfcats', array('RegionID' => $region->id));
						foreach ($query->result() as $row){
							echo "<li>".$row->CategoryName."</li>";
						}
				   ?>
                </div>
            </div>
        </div>
	
	<?php }?>
    
    </div>


<style>
    .faqHeader {
        font-size: 27px;
        margin: 20px;
    }

    .panel-heading [data-toggle="collapse"]:after {
        font-family: 'Glyphicons Halflings';
        content: "\e072"; /* "play" icon */
        float: right;
        color: #F58723;
        font-size: 18px;
        line-height: 22px;
        /* rotate "play" icon from > (right arrow) to down arrow */
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
    }

    .panel-heading [data-toggle="collapse"].collapsed:after {
        /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
        -webkit-transform: rotate(90deg);
        -moz-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        -o-transform: rotate(90deg);
        transform: rotate(90deg);
        color: #454444;
    }
</style>

<!-- Bootstrap FAQ - END -->