<?php echo $form->messages(); ?>

<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title">User Info</h3>
			</div>
			<div class="box-body">
				<?php echo $form->open(); ?>
                
                <select name="region_id" class="chosen-select" data-placeholder="Select Region" >
                <?php foreach ($regions as $region): ?>
                  <option value="<?php echo $region->id; ?>"><?php echo $region->RegionName; ?></option>
                <?php endforeach; ?>
				</select>
					<?php echo $form->bs3_text('Category No', 'CategoryNumber'); ?>
					<?php echo $form->bs3_text('Category Name', 'CategoryName'); ?>
					<?php echo $form->bs3_text('Basis Size', 'BasisSize'); ?>

					<?php echo $form->bs3_submit(); ?>
					
				<?php echo $form->close(); ?>
			</div>
		</div>
	</div>
	
</div>