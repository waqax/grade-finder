<div class="panel-group" id="accordion">
    
    <?php foreach($regions as $region){ ?>
    <?php 
		$query = $this->db->get_where('classifications', array('RegionID' => $region->id));
		$results = $query->result();
		if($results){
			$add_type= "classification";
			$add_class = "add new classification";
		}else{
			$add_type = "category";
			$add_class = "must add category";
			}
	?>
	
    <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $region->id; ?>"><?= $region->RegionName; ?></a>
                	<a class="pull-right"  href="<?= base_url(); ?>admin/<?= $add_type ?>/create?regionID=<?=$region->id; ?>"><?= $add_class; ?></a>
                    <span style="float:right; margin-right:5%;"><?= count($results); ?> Listings</span>
                </h4>
            </div>
            <div id="collapse<?= $region->id; ?>" class="panel-collapse collapse">
                <div class="panel-body">
                   <?php 
						foreach ($results as $row){
							echo "<li><a href='".base_url()."admin/classification/index1/edit/".$row->ClassificationNumber."'>".$row->ClassificationNumber." | ".$row->ClassificationName."</a></li>";
						}
				   ?>
                </div>
            </div>
        </div>
	
	<?php }?>
    </div>