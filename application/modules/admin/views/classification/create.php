<?php echo $form->messages(); ?>
<?php  
if(isset($_GET['regionID'])){
	$regionID = $_GET['regionID'];
}
?>

<div class="row">

	<div class="col-md-6">
		<div class="box box-primary">
			<div class="box-body">
				<?php echo $form->open(); ?>
                <?php if(isset($_GET['regionID'])){
					$query = $this->db->get_where('regions', array('id' => $regionID ));
					$result = $query->result();
					
					$query_category = $this->db->get_where('categories', array('RegionID' => $regionID ));
					$result_categories = $query_category->result();
					?>
                
                <input type="hidden" name="region_id" id="region_id" value="<?php echo $regionID; ?>"/>
                <div class="form-group"><label for="CategoryNumber">Region:</label>
                	<?php echo  $result[0]->RegionName; ?>
				</div>
                <?php }else{ ?>
                <div class="form-group"><label for="CategoryNumber">Region:</label>
                    <select name="region_id" class="chosen-select" data-placeholder="Select Region" >
                    <?php foreach ($regions as $region): ?>
                      <option value="<?php echo $region->id; ?>"><?php echo $region->RegionName; ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
                <?php } ?>
                <div class="form-group"><label for="CategoryNumber">Category:</label>
                    <select name="CategoryNumber" class="chosen-select" data-placeholder="Select CategoryNumber" >
                    <?php foreach ($result_categories as $result_category): ?>
                      <option value="<?php echo $result_category->CategoryNumber; ?>"><?php echo $result_category->CategoryName; ?></option>
                    <?php endforeach; ?>
                    </select>
                </div>
					<?php echo $form->bs3_text('Class No', 'ClassificationNumber','',array('required' => 'required')); ?>
					<?php echo $form->bs3_text('Class Name', 'ClassificationName','',array('required' => 'required')); ?>
					<?php echo $form->bs3_text('Index 1', 'IndexVar_1'); ?>
                    <?php echo $form->bs3_text('Index 2', 'IndexVar_2'); ?>
                    <?php echo $form->bs3_text('Index 3', 'IndexVar_3'); ?>
                    <?php echo $form->bs3_text('Index 4', 'IndexVar_4'); ?>
                    <?php echo $form->bs3_textarea('Description', 'Description'); ?>
                    <?php echo $form->bs3_textarea('DecidingFactor', 'DecidingFactor'); ?>

					<?php echo $form->bs3_submit("Add"); ?>
					
				<?php echo $form->close(); ?>
			</div>
		</div>
	</div>
	
</div>