<?php if ($this->ion_auth->in_group(array('webmaster', 'regional_super_user', 'admin')) ){ ?>
<div class="row">
	<div class="col-md-6">
		<?php echo modules::run('adminlte/widget/box_open', 'Shortcuts'); ?>
			<?php echo modules::run('adminlte/widget/app_btn', 'fa fa-user', 'Account', 'panel/account'); ?>
			<?php echo modules::run('adminlte/widget/app_btn', 'fa fa-sign-out', 'Logout', 'panel/logout'); ?>
		<?php echo modules::run('adminlte/widget/box_close'); ?>
	</div>

	<div class="col-md-6">
		<?php echo modules::run('adminlte/widget/info_box', 'yellow', $count['users'], 'Users', 'fa fa-users', 'user'); ?>
	</div>
</div>
<div class="col-md-12">
    <form action="<?= $this->config->base_url(); ?>admin/searchcompany" method="POST" id="filtering_form" class="filtering_form" autocomplete="off" accept-charset="utf-8" >
        <div class="sDiv quickSearchBox" id="quickSearchBox">Company:
            <div class="sDiv2">
                Search: <input type="text" class="qsbsearch_fieldox search_text" name="search_text" size="30" id="search_text">
                    <select name="search_field" id="search_field">
                        <!--<option value="">Search all</option>-->
                        <option value="AccountNo">AccountNo</option>
                        <option value="Company">Company</option>
                        <!--<option value="Address1">Address</option>-->
                        <!--<option value="EMail">EMail</option>-->
                    </select>
                <input type="submit" value="Search" class="crud_search" name="crud_search" id="crud_search"><br/>
                Enter the ID and/or select option from dropdown
            </div>
        </div>
	</form>
</div> 
<?php }else{ ?>
<?php 
	if(isset($AccountID)){
		$this->db->select ( 'comp.*,state.StateCode' )
		->from ( 'companies as comp' )
		->join ( 'states state', 'comp.StateID = state.id ', 'left')
		->join ( 'users_relation usr', 'usr.accountNo = comp.AccountNo')
		->where('comp.AccountNo',$AccountID)
		->where('usr.userID',$user->id);
		$query = $this->db->get ();
		$results = $query->result();
	}else{
		$this->db->select ( 'comp.*,state.StateCode' )
		->from ( 'companies as comp' )
		->join ( 'states state', 'comp.StateID = state.id ', 'left')
		->join ( 'users_relation usr', 'usr.accountNo = comp.AccountNo')
		->where('usr.userID',$user->id);
		$query = $this->db->get ();
		$results = $query->result();
	}
?>
<?php include("Search_view.php"); ?>
<?php } ?>