<?php 
class SearchCompany_Model extends CI_Model{
function __construct()
    {
        parent::__construct();
    }

    function search($search_field,$search_text)
    {
        
		$this->db->select ( 'comp.*,state.StateCode,regions.*' )
		->from ( 'companies as comp' )
		->join ( 'states state', 'comp.StateID = state.id ', 'left')
		->join ( 'country', 'comp.CountryID = country.id ', 'right')
		->join ( 'regions', 'country.RegionID = regions.id ', 'right');
		
		//->join ( 'soundtracks s_tracks ', 's_tracks.album_id = album.album_id');
		
		if($search_field == "AccountNo"){
			$this->db->where($search_field,$search_text);
			}
		else{
			$this->db->like($search_field,$search_text);
			//$this->db->group_by('Company'); 
			}
			$this->db->where('parentID',NULL);
			//$this->db->where('Grades',"1");
			//$this->db->where('active',"1");
			
       // $query  =   $this->db->get('companies');
	  $this->db->order_by("regions.RegionName","ASC");
	  // $this->db->order_by("country.CountryName","DESC");
	   $this->db->order_by("Company","ASC");
	   
	  $this->db->limit(25);
	   
	   $query = $this->db->get ();
       return $query->result();
    }

}

?>