<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index1()
	{
		$crud = $this->generate_crud('categories');
		
		$crud->columns('CategoryNumber','CategoryName','BasisSize', 'RegionID');
		$crud->display_as('RegionID','Region');
		//$this->unset_crud_fields('ip_address', 'last_login');

		// only webmaster and admin can change member groups
		//if ($crud->getState()=='list' || $this->ion_auth->in_group(array('webmaster', 'admin')))
		//{
			//$crud->set_relation_n_n('groups', 'users_groups', 'users_roles', 'user_id', 'group_id', 'name');
			$crud->set_relation('RegionID', 'regions', 'RegionName');
		//}



		// disable direct create / delete Frontend User
		//$crud->unset_add();
		//$crud->unset_delete();

		$this->mPageTitle = 'Categories';
		$this->render_crud();
		
		/*$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();

		$this->mPageTitle = 'Category';
		$this->render('category/index');*/
		
	}
	
	// Frontend User CRUD
	public function index()
	{
		
		$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();

		$this->mPageTitle = 'Category';
		$this->render('category/index');
		
	}

	
	// Create Category
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($this->input->post())
		{
			
			
			$data = array(
			'RegionID' => $this->input->post('region_id'),
			'CategoryNumber' => $this->input->post('CategoryNumber'),
			'CategoryName' => $this->input->post('CategoryName'),
			'BasisSize' => $this->input->post('BasisSize')
			);
			//Transfering data to Model
			$cat_id = $this->db->insert('categories', $data);
					
			if ($cat_id)
			{
				// success
				//$data['message'] = 'Data Inserted Successfully';
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				//$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		// get list of Frontend user groups
		//$this->load->model('group_model', 'users_roles');
		//$this->mViewData['users_roles'] = $this->users_roles->get_all();
		
		$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();
		
		$this->mPageTitle = 'Add Category';

		$this->mViewData['form'] = $form;
		$this->render('category/create');
	}
	
}
