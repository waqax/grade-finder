<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SearchCompany extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
		$this->load->model('SearchCompany_model');
		$this->load->helper('cookie');
	}

	// Frontend User CRUD
	public function index()
	{
		
		if($this->input->post('search_field') != "" && $this->input->post('search_text') != ""){
			 $search_field = $this->input->post('search_field');
			 $search_text = $this->input->post('search_text');
			$cookie = array(
			'name'   => 'search_field',
			'value'  => $search_field,
			'expire' => time()+86500,
			'domain' => 'localhost',
			'path'   => '/',
			'prefix' => 'company_',
       		 );
			$cookie1 = array(
				'name'   => 'search_text',
				'value'  => $search_text,
				'expire' => time()+86500,
				'domain' => 'localhost',
				'path'   => '/',
				'prefix' => 'company_',
			);
	
		set_cookie($cookie);
		set_cookie($cookie1);
			
		}else{
			 $search_field = get_cookie("company_search_field");
			 $search_text = get_cookie("company_search_text");
				}
		
			
		$this->mViewData['results']    =   $this->SearchCompany_model->search($search_field,$search_text);
        //$this->load->view('Search_view',$data);
		$this->mPageTitle = 'Search Company';
		$this->render('Search_view');
		}


}
