<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class grade extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('grades');
		//$crud->columns('groups', 'username', 'email', 'first_name', 'last_name', 'active');
		//$this->unset_crud_fields('ip_address', 'last_login');
		
		$crud->columns('GradeNumber','Finish','GradeName', 'ClassificationNumber', 'CategoryNumber','AccountNo1','weightrange','opacityrange');
		//$this->unset_crud_fields('user_Password', 'last_login');
		//$crud->display_as('role_id','Role');
		//$crud->display_as('state_id','State');
		//$crud->display_as('country_id','Country');
		$crud->display_as('CategoryNumber','Category');
		$crud->display_as('AccountNo1','AccountNo');
		//$crud->display_as('RegionID','Region');

		// only webmaster and admin can change member groups
		//if ($crud->getState()=='list' || $this->ion_auth->in_group(array('webmaster', 'admin')))
		//{
			//$crud->set_relation_n_n('groups', 'users_groups', 'users_roles', 'user_id', 'group_id', 'name');
			//$crud->set_relation('Grades', 'gfgrades', 'GradeName');
			$crud->set_relation('ClassificationNumber', 'classifications', 'ClassificationName');
			$crud->set_relation('CategoryNumber', 'categories', 'CategoryName');
			//$crud->set_relation('RegionID', 'regions', 'RegionName');
			//$crud->set_relation('StateID', 'states', 'StateName');
			//$crud->set_relation('CountryID', 'country', 'CountryName');
		//}

		// only webmaster and admin can reset user password
		//if ($this->ion_auth->in_group(array('webmaster', 'admin')))
		//{
			//$crud->add_action('Reset Password', '', 'admin/user/reset_password', 'fa fa-repeat');
		//}

		// disable direct create / delete Frontend User
		//$crud->unset_add();
		//$crud->unset_delete();

		$this->mPageTitle = 'Grades';
		$this->render_crud();
		
		/*$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();

		$this->mPageTitle = 'Category';
		$this->render('category/index');*/
		
	}

	// Create Frontend User
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($this->input->post())
		{
			
			
			/*
			// passed validation
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$identity = empty($username) ? $email : $username;
			$additional_data = array(
				'first_name'	=> $this->input->post('first_name'),
				'last_name'		=> $this->input->post('last_name'),
			);
			$groups = $this->input->post('groups');

			// [IMPORTANT] override database tables to update Frontend Users instead of Admin Users
			$this->ion_auth_model->tables = array(
				'users'				=> 'users',
				'groups'			=> 'users_roles',
				'users_groups'		=> 'users_groups',
				'login_attempts'	=> 'login_attempts',
			);

			// proceed to create user
			$user_id = $this->ion_auth->register($identity, $password, $email, $additional_data, $groups);	
			
			*/
			
			$data = array(
			'RegionID' => $this->input->post('region_id'),
			'CategoryNumber' => $this->input->post('CategoryNumber'),
			'CategoryName' => $this->input->post('CategoryName'),
			'BasisSize' => $this->input->post('BasisSize')
			);
			//Transfering data to Model
			$cat_id = $this->db->insert('categories', $data);
					
			if ($cat_id)
			{
				// success
				//$data['message'] = 'Data Inserted Successfully';
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				//$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		// get list of Frontend user groups
		//$this->load->model('group_model', 'users_roles');
		//$this->mViewData['users_roles'] = $this->users_roles->get_all();
		
		$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();
		
		$this->mPageTitle = 'Create Category';

		$this->mViewData['form'] = $form;
		$this->render('category/create');
	}

}
