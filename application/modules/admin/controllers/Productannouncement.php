<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class productannouncement extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('productannouncement');
		//$this->unset_crud_fields('ip_address', 'last_login');
		
		$crud->columns('AccountNo','productAnnouncement_title', 'productAnnouncement_link', 'productAnnouncement_copy');

		//$crud->display_as('CategoryNumber','Category');


		$this->mPageTitle = 'Product Announcement';
		$this->render_crud();
		
		/*$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();

		$this->mPageTitle = 'Category';
		$this->render('category/index');*/
		
	}

	// Create Frontend User
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($this->input->post())
		{
			
			$data = array(
			'AccountNo' => $this->input->post('AccountNo'),
			'productAnnouncement_title' => $this->input->post('productAnnouncement_title'),
			'productAnnouncement_link' => $this->input->post('productAnnouncement_link'),
			'productAnnouncement_copy' => $this->input->post('productAnnouncement_copy')
			);
			//Transfering data to Model
			$cat_id = $this->db->insert('productannouncement', $data);
					
			if ($cat_id)
			{
				// success
				//$data['message'] = 'Data Inserted Successfully';
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				//$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		// get list of Frontend user groups
		//$this->load->model('group_model', 'users_roles');
		//$this->mViewData['users_roles'] = $this->users_roles->get_all();
		
		//$this->load->model('region_model', 'region');
		//$this->mViewData['regions'] = $this->region->get_all();
		
		$this->mPageTitle = 'Add Product Announcement';

		$this->mViewData['form'] = $form;
		$this->render('productannouncement/create');
	}
	
	// Approve
	public function approved($ID='')
	{
		$query_company = $this->db->get_where('productannouncement', array('id' => $ID));
		$get_company = $query_company->result();
		//$active_db = $get_company[0]->active;
		//if($active_db == 0) $active = 1;
		//else $active = 0;
		
		$data=array('active' => '1');
		$this->db->where('id',$ID);
		$this->db->update('productannouncement',$data);
		redirect($this->mModule.'/searchcompany/index/'.$get_company[0]->AccountNo);
	}


}
