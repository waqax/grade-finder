<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classification extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index1()
	{
		$crud = $this->generate_crud('classifications');
		//$crud->columns('groups', 'username', 'email', 'first_name', 'last_name', 'active');
		//$this->unset_crud_fields('ip_address', 'last_login');
		
		$crud->columns('ClassificationNumber','ClassificationName','CategoryNumber', 'RegionID', 'Description','DecidingFactor');
		//$this->unset_crud_fields('user_Password', 'last_login');
		//$crud->display_as('role_id','Role');
		//$crud->display_as('state_id','State');
		//$crud->display_as('country_id','Country');
		$crud->display_as('CategoryNumber','Category');
		$crud->display_as('RegionID','Region');

		// only webmaster and admin can change member groups
		//if ($crud->getState()=='list' || $this->ion_auth->in_group(array('webmaster', 'admin')))
		//{
			//$crud->set_relation_n_n('groups', 'users_groups', 'users_roles', 'user_id', 'group_id', 'name');
			//$crud->set_relation('Grades', 'gfgrades', 'GradeName');
			$crud->set_relation('CategoryNumber', 'categories', 'CategoryName');
			$crud->set_relation('RegionID', 'regions', 'RegionName');
			//$crud->set_relation('StateID', 'states', 'StateName');
			//$crud->set_relation('CountryID', 'country', 'CountryName');
		//}


		// disable direct create / delete Frontend User
		//$crud->unset_add();
		//$crud->unset_delete();

		$this->mPageTitle = 'Classifications';
		$this->render_crud();

		
	}
	
	// Frontend User CRUD
	public function index()
	{
		
		$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();

		$this->mPageTitle = 'Classification';
		$this->render('classification/index');
		
	}
	
	// Create
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($this->input->post()){
			
			$data = array(
			'RegionID' => $this->input->post('region_id'),
			'CategoryNumber' => $this->input->post('CategoryNumber'),
			'ClassificationName' => $this->input->post('ClassificationName'),
			'IndexVar_1' => $this->input->post('IndexVar_1'),
			'IndexVar_2' => $this->input->post('IndexVar_2'),
			'IndexVar_3' => $this->input->post('IndexVar_3'),
			'IndexVar_4' => $this->input->post('IndexVar_4'),
			'Description' => $this->input->post('Description'),
			'DecidingFactor' => $this->input->post('DecidingFactor'),
			);
			//Transfering data to Model
			$class_id = $this->db->insert('classifications', $data);		
			if ($class_id){
				// success
				//$data['message'] = 'Data Inserted Successfully';
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				//$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		// get list of Frontend user groups
		//$this->load->model('group_model', 'users_roles');
		//$this->mViewData['users_roles'] = $this->users_roles->get_all();
		
		$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();
		
		$this->mPageTitle = 'Add Classification';

		$this->mViewData['form'] = $form;
		$this->render('classification/create');
	}


}
