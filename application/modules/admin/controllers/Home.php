<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {
	

	public function index($id='')
	{
		$this->load->model('user_model', 'users');
		if($id != ""){
			$this->mViewData['AccountID'] = $id;
			}
		$this->mViewData['count'] = array(
			'users' => $this->users->count_all(),
		);
		$this->render('login');
	}
}
