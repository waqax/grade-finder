<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	// Frontend User CRUD
	public function index()
	{
		$crud = $this->generate_crud('companies');
		//$crud->columns('groups', 'username', 'email', 'first_name', 'last_name', 'active');
		//$this->unset_crud_fields('ip_address', 'last_login');
		
		$crud->columns('AccountNo','Grades','Company', 'Address1','CountryID','EMail');
		
		$crud->add_action('Action', '', 'admin/company/approved','fa fa-check-square comp-active');
		
		$crud->display_as('Address1','Address');
		$crud->display_as('StateID','State');
		$crud->display_as('CountryID','Country');
		$crud->display_as('Phone1','Phone');
		
		if (!$this->ion_auth->in_group(array('webmaster','regional_super_user', 'admin'))){
			$this->unset_crud_fields('active');
		}
		// only webmaster and admin can change member groups
		//if ($crud->getState()=='list' || $this->ion_auth->in_group(array('webmaster', 'admin')))
		//{
			//$crud->set_relation_n_n('groups', 'users_groups', 'users_roles', 'user_id', 'group_id', 'name');
			$crud->set_relation('Section', 'sectiontype', 'sectionName');
			$crud->set_relation('Grades', 'grades', 'GradeName');
			$crud->set_relation('StateID', 'states', 'StateCode');
			$crud->set_relation('Office', 'offices', 'OfficeName');
			$crud->set_relation('CountryID', 'country', 'CountryName');
		//}

		// only webmaster and admin can access
		//if ($this->ion_auth->in_group(array('webmaster', 'admin'))){}

		// disable direct create / delete Frontend User
		//$crud->unset_add();
		//$crud->unset_delete();

		$this->mPageTitle = 'Companies';
		$this->render_crud();
		
		/*$this->load->model('region_model', 'region');
		$this->mViewData['regions'] = $this->region->get_all();

		$this->mPageTitle = 'Category';
		$this->render('category/index');*/
		
	}
	
	//Company Detail
	public function detail($AccountNo='')
	{
		$this->mViewData['AccountID'] = $AccountNo;
			
		$this->render('company/detail');
	}
	
	// Create Frontend User
	public function create()
	{
		$form = $this->form_builder->create_form();

		if ($this->input->post())
		{
			
			$data = array(
			'Company' => $this->input->post('Company'),
			'Division' => $this->input->post('Division'),
			'Address1' => $this->input->post('Address1'),
			'CountryID' => $this->input->post('CountryID'),
			'StateID' => $this->input->post('StateID'),
			'ZipPostalCode' => $this->input->post('ZipPostalCode'),
			'Phone1' => $this->input->post('Phone1'),
			'Fax' => $this->input->post('Fax'),
			'EMail' => $this->input->post('EMail'),
			'HomePage' => $this->input->post('HomePage'),
			'Certification' => $this->input->post('Certification'),
			'companyDescription' => $this->input->post('companyDescription'),
			'Personnel_1' => $this->input->post('Personnel_1'),
			'Personnel_2' => $this->input->post('Personnel_2'),
			'Personnel_3' => $this->input->post('Personnel_3'),
			'Personnel_4' => $this->input->post('Personnel_4'),
			'Personnel_5' => $this->input->post('Personnel_5'),
			'parentID' => $this->input->post('parentID'),
			'AdPgNo' => $this->input->post('AdPgNo'),
			'Grades' => $this->input->post('Grades'),
			'globalheadquarters' => $this->input->post('globalheadquarters'),
			'corporateheadquarters' => $this->input->post('corporateheadquarters'),
			'Section' => $this->input->post('Section'),
			'Office' => $this->input->post('Office'),
			);
			//Transfering data to Model
			$cat_id = $this->db->insert('companies', $data);
					
			if ($cat_id)
			{
				// success
				//$data['message'] = 'Data Inserted Successfully';
				$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);

				// directly activate user
				//$this->ion_auth->activate($user_id);
			}
			else
			{
				// failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
			}
			refresh();
		}

		// get list of Frontend user groups
		//$this->load->model('group_model', 'users_roles');
		//$this->mViewData['users_roles'] = $this->users_roles->get_all();
		
		$query_offices = $this->db->get_where('country');
		$this->mViewData['Countries'] = $query_offices->result();
		
		$query_states = $this->db->get_where('states');
		$this->mViewData['states'] = $query_states->result();
		
		$query_section = $this->db->get_where('sectiontype');
		$this->mViewData['sections'] = $query_section->result();
		
		$query_offices = $this->db->get_where('offices');
		$this->mViewData['offices'] = $query_offices->result();
		
		//$this->load->model('region_model', 'region');
		//$this->mViewData['regions'] = $this->region->get_all();
		
		$this->mPageTitle = 'Company Location Update Area';
		
		$messages = $this->ion_auth->messages();
		$this->system_message->set_success($messages);

		$this->mViewData['form'] = $form;
		$this->render('company/create');
	}


	// Approve
	public function approved($AccountNo='')
	{
		$query_company = $this->db->get_where('companies', array('AccountNo' => $AccountNo));
		$get_company = $query_company->result();
		//$active_db = $get_company[0]->active;
		//if($active_db == 0) $active = 1;
		//else $active = 0;
		
		$data=array('active' => '1');
		$this->db->where('AccountNo',$AccountNo);
		$this->db->update('companies',$data);
		redirect($this->mModule.'/searchcompany/index/'.$get_company[0]->parentID);
	}


}
