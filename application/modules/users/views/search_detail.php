<div class="alert alert-danger">
<strong>IMPORTANT MESSAGE: PLEASE READ!</strong>
The grade information available on this page is shown as an example and is only a portion of the information available to full subscribers.
There are 18 more grades available in this classification: Pressure Sensitive Label Stock Vinyl.
In order to view all grade information, you must login first. If you have an established account, please login now.
To begin your free trial, please click here.
</div>

<div id="results">
<h2><?php echo $categoryName; ?></h2>
<h4><?php echo $classificationName; ?></h4>
<p><?php echo $description; ?></p>
<table id="myDataTable" class="table table-striped">
    <thead>
        <tr>
            <th>Brand Name/Manufacturer</th>
            <th>Finish</th>
            <th>Target Brightness</th>
            <th>Alkaline</th>
            <th>Target Gloss</th>
            <th>Weight Range</th>
            <th>Caliper Range</th>
            <th>Opacity Range</th>
            <th>Color</th>
            <th>Size</th>
            <th>Furnish</th>
            <th>Recycles</th>
            <th>Certificaiton</th>
            <th>Other informaiton</th>
        </tr>

        <?php
        //var_dump($results);
            foreach($results as $result){
           //    echo '<pre>'.var_dump($result).'</pre>';
                echo '<tr>';
                    echo '<td><a href="'.base_url("users/searchcompany/detail").'?Account='.$result->AccountNo.'">'.$result->GradeName.' - '.$result->Company.'</a></td>';
                    echo '<td><a href="">'.$result->Finish.'</a></td>'; 
                    echo '<td>'.$result->Brightness.'</td>';
                    echo '<td>'.$result->Alkaline.'</td>';
                    echo '<td>'.$result->gloss.'</td>';
                    echo '<td>'.$result->weightrange.'</td>';
                    echo '<td>'.$result->caliperrange.'</td>';
                    echo '<td>'.$result->opacityrange.'</td>';
                    echo '<td>'.$result->WhiteColor.'</td>';    
                    echo '<td>'.$result->Furnish.'</td>';  
                    echo '<td>'.$result->WhiteColor.'</td>';  
                    echo '<td>'.$result->WhiteColor.'</td>';  
                    echo '<td>'.$result->Certification.'</td>';  
                    echo '<td>'.$result->otherinfo.'</td>';  
                echo '</tr>';
            }
        ?>
    </thead>
    <tbody></tbody>
</table>
</div>
<div class="alert alert-danger">
<strong>IMPORTANT MESSAGE: PLEASE READ!</strong>
The grade information available on this page is shown as an example and is only a portion of the information available to full subscribers.
There are 18 more grades available in this classification: Pressure Sensitive Label Stock Vinyl.
In order to view all grade information, you must login first. If you have an established account, please login now.
To begin your free trial, please click here.
</div>