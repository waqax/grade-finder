<?php 
class SearchFrontEnd extends CI_Model{
function __construct()
    {
        parent::__construct();
    }

    function search($region,$class,$grade)
    {
        
        $this->db->select ( '*' )
        ->from ( 'companies as comp')
        ->join ( 'country', 'comp.CountryID = country.id ')
        ->join ( 'regions', 'country.RegionID = regions.id ');
        $this->db->join('grades','grades.AccountNo1 = comp.AccountNo');
        $this->db->join('classifications', 'grades.ClassificationNumber = classifications.ClassificationNumber');

        $this->db->like('GradeName',$grade);
        $this->db->like('ClassificationName',$class);
        //->join ( 'soundtracks s_tracks ', 's_tracks.album_id = album.album_id');

        // $query  =   $this->db->get('companies');
        $this->db->order_by("regions.RegionName","ASC");
        // $this->db->order_by("country.CountryName","DESC");
        $this->db->order_by("GradeName","ASC");

        $this->db->limit(200);

        $query = $this->db->get ();
        return $query->result();
    }
    function searchGrades($class,$cat,$reg){
        $this->db->select ( '*' )
        ->from ( 'companies as comp')
        ->join ( 'country', 'comp.CountryID = country.id ')
        ->join ( 'regions', 'country.RegionID = regions.id ');
        $this->db->join('grades','grades.AccountNo1 = comp.AccountNo');
        $this->db->join ('categories','categories.CategoryNumber = grades.CategoryNumber');
        $this->db->join('classifications', 'grades.ClassificationNumber = classifications.ClassificationNumber');
        $this->db->where('classifications.ClassificationNumber',$class);
        if($reg!=0)
        $this->db->where('regions.id',$reg);
        $this->db->where('classifications.CategoryNumber',$cat);
        //->join ( 'soundtracks s_tracks ', 's_tracks.album_id = album.album_id');
        // $query  =   $this->db->get('companies');
       // $this->db->order_by("regions.RegionName","ASC");
        // $this->db->order_by("country.CountryName","DESC");
        $this->db->order_by("GradeName","ASC");
       // $this->db->limit(25);
        $query = $this->db->get();
        return $query->result();
    }
    function searchCompany($comp,$reg){
        $this->db->select('*')
        ->from ('companies as comp')
        ->join ( 'country', 'comp.CountryID = country.id ')
        ->join ( 'regions', 'country.RegionID = regions.id ')
        ->like ('comp.Company',$comp);
       if($reg!=0)
        $this->db->like('regions.id',$reg);
        $this->db->order_by('comp.Company');
        $this->db->join('states','states.id = comp.StateID');
        $this->db->join('sectiontype','sectiontype.id = comp.Section');
        $query = $this->db->get();
        return $query->result();

    }
      function searchCompanyDetail($acc){
        $this->db->select('*')
        ->from ('companies as comp')
        ->join ( 'country', 'comp.CountryID = country.id ')
        ->join ( 'regions', 'country.RegionID = regions.id ')
        ->like ('comp.AccountNo',$acc);
        $this->db->order_by('comp.Company');
        $this->db->join('states','states.id = comp.StateID','LEFT');
        $this->db->join('sectiontype','sectiontype.id = comp.Section','LEFT');
        $query = $this->db->get();
   
        return $query->row_array();

    }
    
     function searchGradesForCompany($comp){
        $this->db->select('*')
        ->from ('grades');
        $this->db->join ('categories','categories.CategoryNumber = grades.CategoryNumber');
        $this->db->join('classifications', 'grades.ClassificationNumber = classifications.ClassificationNumber');
        $this->db->where('grades.AccountNo1',$comp);
        $this->db->order_by('grades.GradeName');
        $query = $this->db->get();
        return $query->result();

    }
}

?>