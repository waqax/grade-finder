<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Searchcompany extends Users_controller {
	
    public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
        
	}

	public function index($id='')
	{   $this->mPageTitle = 'Search Results';
        $comp = $this->input->post('name');
		$reg = $this->input->post('region_comp');  
        $this->load->model('SearchFrontEnd');
		$results = $this->SearchFrontEnd->searchCompany($comp,$reg);
		$this->mViewData['results'] = $results;
		$this->render('comp_results');
	}
	public function detail(){
		$this->mPageTitle = 'Company Detail';
		$acc = $this->input->get('Account');
		$this->load->model('SearchFrontEnd'); 
		$results = $this->SearchFrontEnd->searchCompanyDetail($acc); 
		$this->mViewData['results'] = $results;
		$this->mViewData['grades'] = $this->SearchFrontEnd->searchGradesForCompany($acc);
		$this->render('company_detail');
	}
}
