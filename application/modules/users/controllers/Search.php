<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends Users_controller {
	
    public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
        
	}

	public function index($id='')
	{   $this->mPageTitle = 'Search Results';
        $grade = $this->input->post('name');
        $region = $this->input->post('region');
        $class = $this->input->post('classification');
        $this->load->model('SearchFrontEnd');
        $this->mViewData['results'] = $this->SearchFrontEnd->search($region,$class,$grade);  
		$this->render('results');
	}
	public function detail(){
		
		$this->mPageTitle = 'Grade Results';
		$class = $this->input->get('ClassNo');
		$cat = $this->input->get('CatNo');
		$region = $this->input->get('region');
		$this->load->model('SearchFrontEnd'); 
		$results = $this->SearchFrontEnd->searchGrades($class,$cat,$region); 
		$this->mViewData['results'] = $results;
		$this->mViewData['categoryName'] = $results[0]->CategoryName;
		$this->mViewData['classificationName'] = $results[0]->ClassificationName;
		$this->mViewData['description'] = $results[0]->Description;
		$this->render('search_detail');
	}
}
